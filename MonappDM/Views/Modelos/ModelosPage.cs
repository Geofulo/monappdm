﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace MonappDM
{
	public class ModelosPage : ContentPage
	{
		List<ModeloModel> Modelos;

		ToolbarItem AddItem;

		ListView ModelosListView;

		public ModelosPage ()
		{
			Title = "Modelos";

			getModelos ();
			createItems ();
			createElements ();
			setListeners ();

			setContent ();
		}

		void getModelos ()
		{
			Modelos = App.WSRest.GetModelos ();	
		}

		void createItems ()
		{
			AddItem = new ToolbarItem {
				Text = "Crear",
			};

			ToolbarItems.Add (AddItem);
		}

		void createElements ()
		{
			ModelosListView = new ListView {
				ItemsSource = Modelos,
				ItemTemplate = new DataTemplate(typeof(ModelosTemplate)),
				RowHeight = 50,
				IsPullToRefreshEnabled = true,
			};
		}

		void setListeners ()
		{
			AddItem.Clicked += (sender, e) => {
				Navigation.PushAsync (new CreateModeloPage());
			};

			ModelosListView.Refreshing += (sender, e) => {
				getModelos ();
				ModelosListView.ItemsSource = Modelos;
				setContent();

				ModelosListView.IsRefreshing = false;
			};

			ModelosListView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null)
					return;

				var item = e.SelectedItem as ModeloModel;

				ModelosListView.SelectedItem = null;

				Navigation.PushAsync (new DetailModeloPage (item));
			};
		}

		void setContent ()
		{
			if (Modelos.Count > 0)
				Content = ModelosListView;
			else 
			{
				Content = new Label {
					Text = "No tienes ningún modelo",
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand,
				};

			}
		}
	}
}

