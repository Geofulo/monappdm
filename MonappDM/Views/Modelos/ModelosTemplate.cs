﻿using System;
using Xamarin.Forms;

namespace MonappDM
{
	public class ModelosTemplate : ViewCell
	{
		Label NombreLabel;
		Label FechaLabel;
		Switch IsAvailableSwitch;

		public ModelosTemplate ()
		{
			createElements ();
			setBindings ();
			setListeners ();

			View = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(10),
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = {
					NombreLabel,
					FechaLabel,
					IsAvailableSwitch
				}
			};
		}

		void createElements ()
		{
			NombreLabel = new Label {
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			FechaLabel = new Label {
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			IsAvailableSwitch = new Switch {				
				HorizontalOptions = LayoutOptions.EndAndExpand,
			};
		}

		void setBindings ()
		{
			NombreLabel.SetBinding (Label.TextProperty, "Nombre");
			FechaLabel.SetBinding (Label.TextProperty, "FechaCreacion");
			IsAvailableSwitch.SetBinding (Switch.IsToggledProperty, "IsAvailable");
		}

		void setListeners ()
		{
			IsAvailableSwitch.Toggled += (sender, e) => {
				var modelo = (ModeloModel) this.BindingContext;
				modelo.IsAvailable = IsAvailableSwitch.IsToggled;

				App.WSRest.UpdateModelo (modelo);
			};
		}
	}
}

