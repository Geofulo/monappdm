﻿using System;
using Xamarin.Forms;

namespace MonappDM
{
	public class CreateModeloPage : ContentPage
	{
		ToolbarItem SaveItem;

		Entry NombreEntry;
		Editor DescripcionEditor;

		StackLayout Stack;

		public CreateModeloPage ()
		{
			Title = "Crear Modelo";

			createItems ();
			createElements ();
			setLayouts ();
			setListeners ();

			Content = Stack;
		}

		void createItems ()
		{
			SaveItem = new ToolbarItem {
				Text = "Guardar",
			};

			ToolbarItems.Add (SaveItem);
		}

		void createElements ()
		{
			NombreEntry = new Entry {
				Placeholder = "Escribe el nombre del modelo",
				HeightRequest = 40,
			};
			DescripcionEditor = new Editor {
				HeightRequest = 80,
			};
		}

		void setLayouts ()
		{
			Stack = new StackLayout {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Children = {
					new StackLayout {
						Padding = new Thickness(0, 10),
						Children = {
							new Label {
								Text = "Nombre",
							},
							NombreEntry,
						}
					},
					new StackLayout {
						Padding = new Thickness(0, 10),
						Children = {
							new Label {
								Text = "Descripción",
							},
							DescripcionEditor,
						}
					}
				}
			};
		}

		void setListeners ()
		{
			SaveItem.Clicked += (sender, e) => {
				string nombre = NombreEntry.Text;
				string des = DescripcionEditor.Text;
				if (!string.IsNullOrEmpty(nombre)) 
				{
					var modeloCreated = new ModeloModel {
						Nombre = nombre,
						Descripcion = des
					};

					if (App.WSRest.CreateModelo (modeloCreated)) 
					{
						Navigation.PopAsync();
					}
					else 
					{
						DisplayAlert ("", "Ocurrió un error al intentar crear el modelo", "OK");
					}
				}
				else 
				{
					DisplayAlert ("", "Debes ingresas toda la información", "OK");
				}
			};
		}
	}
}

