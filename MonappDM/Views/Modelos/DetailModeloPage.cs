﻿using System;
using Xamarin.Forms;

namespace MonappDM
{
	public class DetailModeloPage : ContentPage
	{
		ModeloModel Modelo;

		ToolbarItem EditItem;
		ToolbarItem SaveItem;

		Entry NombreEntry;
		Editor DescripcionEditor;
		Switch IsAvailableSwitch;

		StackLayout Stack;

		public DetailModeloPage (ModeloModel Modelo)
		{
			this.Modelo = Modelo;

			Title = Modelo.Nombre;

			createItems ();
			createElements ();
			setLayouts ();
			setListeners ();

			Content = Stack;
		}

		void createItems ()
		{
			EditItem = new ToolbarItem {
				Text = "Editar"
			};

			SaveItem = new ToolbarItem {
				Text = "Guardar",
			};

			ToolbarItems.Add (EditItem);
		}

		void createElements ()
		{
			NombreEntry = new Entry {
				Text = Modelo.Nombre,
				IsEnabled = false,
				HeightRequest = 40,
			};

			DescripcionEditor = new Editor {
				Text = Modelo.Descripcion,
				IsEnabled = false,
				HeightRequest = 80,
			};

			IsAvailableSwitch = new Switch {
				IsToggled = Modelo.IsAvailable,
				IsEnabled = false,
			};
		}
			
		void setLayouts ()
		{
			Stack = new StackLayout {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Children = {
					new StackLayout {
						Padding = new Thickness(0, 10),
						Children = {
							new Label {
								Text = "Nombre",
							},
							NombreEntry,
						}
					},
					new StackLayout {
						Padding = new Thickness(0, 10),
						Children = {
							new Label {
								Text = "Descripción",
							},
							DescripcionEditor,
						}
					},
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Spacing = 10,
						Padding = new Thickness(0, 10),
						Children = {
							new Label {
								Text = "¿Está disponible?",
								VerticalOptions = LayoutOptions.CenterAndExpand
							},
							IsAvailableSwitch,
						}
					}
				}
			};
		}

		void setListeners ()
		{
			EditItem.Clicked += (sender, e) => {
				ToolbarItems.Clear ();
				ToolbarItems.Add (SaveItem);

				NombreEntry.IsEnabled = true;
				DescripcionEditor.IsEnabled = true;
				IsAvailableSwitch.IsEnabled = true;
			};

			SaveItem.Clicked += (sender, e) => {
				string nombre = NombreEntry.Text;
				string des = DescripcionEditor.Text;
				bool isAva = IsAvailableSwitch.IsToggled;

				if (!string.IsNullOrEmpty(nombre)) 
				{
					Modelo.Nombre = nombre;
					Modelo.Descripcion = des;
					Modelo.IsAvailable = isAva;

					if (App.WSRest.UpdateModelo (Modelo))
						Navigation.PopAsync();
				}
				else 
				{
					DisplayAlert ("", "Debes ingresas toda la información", "OK");
				}
			};

		}
	}
}

