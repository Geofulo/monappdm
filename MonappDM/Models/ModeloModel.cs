﻿using System;
using Telerik.Everlive.Sdk.Core.Model.Base;
using Telerik.Everlive.Sdk.Core.Serialization;

namespace MonappDM
{
	[ServerType("Modelo")]
	public class ModeloModel : DataItem
	{
		private string nombre;
		private string descripcion;
		private bool isAvailable;

		public string Nombre { 
			get {
				return this.nombre;
			}
			set { 
				this.SetProperty (ref this.nombre, value, "Nombre");
			}
		}

		public string Descripcion { 
			get {
				return this.descripcion;
			}
			set { 
				this.SetProperty (ref this.descripcion, value, "Description");
			}
		}

		public bool IsAvailable { 
			get {
				return this.isAvailable;
			}
			set { 
				this.SetProperty (ref this.isAvailable, value, "IsAvailable");
			}
		}

		[ServerIgnore]
		public string FechaCreacion {
			get {
				var date = this.CreatedAt;
				return date.Day + "/" + date.Month + "/" + date.Year;
			}
		}

	}
}

