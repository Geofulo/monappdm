﻿using System;
using Telerik.Everlive.Sdk.Core;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MonappDM
{
	public class RestService
	{
		EverliveApp EveApp;

		public RestService ()
		{
			EverliveAppSettings appSettings = new EverliveAppSettings {
				AppId = "m70cq3ny9uiyk5c5",
				TestMode = true,
				//UseHttps = false 		// My license no support https connections 
			};
			EveApp = new EverliveApp (appSettings);
		}

		// :::::::::: MODELOS ::::::::::

		public List<ModeloModel> GetModelos () 
		{
			System.Diagnostics.Debug.WriteLine ("GetModelos");

			var tcs = new TaskCompletionSource<List<ModeloModel>> ();

			var res = new List<ModeloModel> ();

			new Task ( async () => {
				try {
					var result = await EveApp.WorkWith().Data<ModeloModel>().GetAll().ExecuteAsync();

					foreach (var r in result)
					{
						res.Add (r);
					}

					System.Diagnostics.Debug.WriteLine (res.Count + " modelos");

					tcs.SetResult (res);
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine (e.Message);
					tcs.SetResult (null);
				}
			}).Start ();

			return tcs.Task.Result;
		}

		public bool CreateModelo (ModeloModel modelo)
		{
			System.Diagnostics.Debug.WriteLine ("CreateModelo");

			var tcs = new TaskCompletionSource<bool> ();

			new Task ( async () => {
				try {
					var result = await EveApp.WorkWith().Data<ModeloModel>().Create(modelo).ExecuteAsync();

					System.Diagnostics.Debug.WriteLine ("modelo " + result.Id + " creado");
					tcs.SetResult (true);
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine (e.Message);
					tcs.SetResult (false);
				}
			}).Start ();

			return tcs.Task.Result;
		}

		public bool UpdateModelo (ModeloModel modelo)
		{
			System.Diagnostics.Debug.WriteLine ("UpdateModelo");

			var tcs = new TaskCompletionSource<bool> ();

			new Task ( async () => {
				try {					
					var result = await EveApp.WorkWith().Data<ModeloModel>().Update(modelo).ExecuteAsync();

					if (result)
						System.Diagnostics.Debug.WriteLine ("modelo " + modelo.Id + " actualizado");
					else
						MostrarAlerta("", "Ocurrió un error al intentar actualizar el modelo", "OK");
					
					tcs.SetResult (result);
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine (e.Message);
					tcs.SetResult (false);
				}
			}).Start ();

			return tcs.Task.Result;
		}

		public void MostrarAlerta (string title, string msg, string btn) 
		{
			Device.BeginInvokeOnMainThread (() => {				
				App.Current.MainPage.DisplayAlert (title, msg, btn);
			});
		}
	}
}

