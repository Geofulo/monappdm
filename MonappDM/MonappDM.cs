﻿using System;

using Xamarin.Forms;

namespace MonappDM
{
	public class App : Application
	{
		public static RestService WSRest;

		public App ()
		{
			WSRest = new RestService ();

			MainPage = new NavigationPage(new TabbedPage {
				Title = "Monapp DM",
				Children = {
					new ModelosPage(),
					new PedidosPage()
				}
			});
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

	}
}

